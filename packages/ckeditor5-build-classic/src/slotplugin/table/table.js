import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import Widget from '@ckeditor/ckeditor5-widget/src/widget';
import UpcastWriter from '@ckeditor/ckeditor5-engine/src/view/upcastwriter';
import { toWidget, viewToModelPositionOutsideModelElement } from '@ckeditor/ckeditor5-widget/src/utils';

import { StyleCommand, HeaderColor, BodyColor } from './commands';
import { TABLE_CONTAINER, TABLE_SCHEMA } from './constants';
import TableTools from './tabletools';

export default class SlotCustomTable extends Plugin {
	static get requires() {
		return [ Widget ];
	}

	static get pluginName() {
		return 'custom-table';
	}

	init() {
		const { editor } = this

		this._defineSchema();
		this._defineConverters();
		this._defineClipboardInputOutput();

		editor.editing.mapper.on(
			'viewToModelPosition',
			viewToModelPositionOutsideModelElement(editor.model,
				(element) => element.hasClass(TABLE_CONTAINER))
		);

		editor.commands.add('changeTableStyle', new StyleCommand(editor))
		editor.commands.add('changeTableHeaderColor', new HeaderColor(editor))
		editor.commands.add('changeTableBodyColor', new BodyColor(editor))
	}

	_defineSchema() {
		this.editor.model.schema.register(TABLE_SCHEMA, {
			allowWhere: '$block',
			isInline: false,
			isObject: true,
			allowAttributes: ['alias', 'data-workers-table', 'data-tasks-table', 'data-concerns-table']
		});
	}

	_defineConverters() {
		const { conversion } = this.editor;

		// Loading the data to the editor
		// Data-to-model conversion
		conversion.for('upcast').elementToElement({
			view: {
				name: 'div',
				classes: [TABLE_CONTAINER]
			},
			model: (viewElement, { writer }) => {
				console.log('table upcast')
				return writer.createElement(
					TABLE_SCHEMA,
					SlotCustomTable.getDataFromView(viewElement)
				)
			}
		})

		// Retrieving the data from the editor
		// Model-to-data conversion
		conversion.for('dataDowncast').elementToElement({
			model: TABLE_SCHEMA,
			view: (modelItem, { writer }) => {
				console.log('table dataDowncast')
				return createTableView(modelItem, writer)
			}
		})


		// Rendering the editor content to the user for editing
		// Model-to-view conversion
		conversion.for('editingDowncast').elementToElement({
			model: TABLE_SCHEMA,
			view: (modelItem, { writer }) => {
				console.log('editingDowncast')
				return toWidget(createTableViewWidget(modelItem, writer), writer)
			}
		})

		function createTableView(modelItem, viewWriter) {
			const alias = modelItem.getAttribute('alias');

			const attributes = {
				class: TABLE_CONTAINER,
			}

			if (modelItem.hasAttribute('data-workers-table')) {
				attributes['data-workers-table'] = modelItem.getAttribute('data-workers-table');
			} else if (modelItem.hasAttribute('data-concerns-table')) {
				attributes['data-concerns-table'] = modelItem.getAttribute('data-concerns-table');
			} else if (modelItem.hasAttribute('data-tasks-table')) {
				attributes['data-tasks-table'] = modelItem.getAttribute('data-tasks-table');
			}

			const cardView = viewWriter.createContainerElement('div', attributes);

			viewWriter.insert(viewWriter.createPositionAt(cardView, 0), viewWriter.createText(alias));

			return cardView;
		}

		function createTableViewWidget(modelItem, viewWriter) {
			const alias = modelItem.getAttribute('alias').slice(1, -1);

			const containerAttrs = {
				class: TABLE_CONTAINER,
			};
			let styleString = null;

			if (modelItem.hasAttribute('data-workers-table')) {
				styleString = modelItem.getAttribute('data-workers-table');
				containerAttrs['data-workers-table'] = styleString;
			} else if (modelItem.hasAttribute('data-concerns-table')) {
				styleString = modelItem.getAttribute('data-concerns-table');
				containerAttrs['data-concerns-table'] = styleString;
			} else if (modelItem.hasAttribute('data-tasks-table')) {
				styleString = modelItem.getAttribute('data-tasks-table');
				containerAttrs['data-tasks-table'] = styleString;
			}

			// Editable View:
			// <table>
			// 	<tr>
			// 		<th>Header</th>
			// 	</tr>
			// 	<tr>
			// 		<td>Body</td>
			// 	</tr>
			// </table>

			const style = JSON.parse(styleString);

			const tableAttrs = {
				style: TableTools.generateStyleFromObject(style.table),
			};
			const headerAttrs = {
				style: TableTools.generateStyleFromObject(style.th),
			};
			const bodyAttrs = {
				style: TableTools.generateStyleFromObject(style.td),
			};

			const cardView = viewWriter.createContainerElement('div', containerAttrs);
			const table = viewWriter.createContainerElement('table', tableAttrs);
			const headerRow = viewWriter.createContainerElement('tr');
			const bodyRow = viewWriter.createContainerElement('tr');
			const header = viewWriter.createContainerElement('th', headerAttrs);
			const body = viewWriter.createContainerElement('td', bodyAttrs);

			const headerText = viewWriter.createText(alias);
			const bodyText = viewWriter.createText('Body');

			viewWriter.insert(viewWriter.createPositionAt(cardView, 0), table);
			viewWriter.insert(viewWriter.createPositionAt(table, 0), bodyRow);
			viewWriter.insert(viewWriter.createPositionAt(table, 0), headerRow);

			viewWriter.insert(viewWriter.createPositionAt(bodyRow, 0), body);
			viewWriter.insert(viewWriter.createPositionAt(body, 0), bodyText);

			viewWriter.insert(viewWriter.createPositionAt(headerRow, 0), header);
			viewWriter.insert(viewWriter.createPositionAt(header, 0), headerText);

			return cardView;
		}
	}

	// Integration with the clipboard pipeline
	_defineClipboardInputOutput() {
		const { view } = this.editor.editing;
		const viewDocument = view.document;

		// Processing pasted or dropped content
		this.listenTo(viewDocument, 'clipboardInput', (evt, data) => {
			if (data.content) return;

			const tableData = data.dataTransfer.getData('table-slot-alias');

			if (tableData) {
				console.log('table clipboardInput')

				const writer = new UpcastWriter(viewDocument);
				const fragment = writer.createDocumentFragment();

				const { alias } = JSON.parse(tableData);

				const attributes = {
					class: TABLE_CONTAINER,
				}

				switch (alias) {
					case '%workers table%': {
						attributes['data-workers-table'] = SlotCustomTable.generateStyle()
						break;
					}
					case '%tasks table%': {
						attributes['data-tasks-table'] = SlotCustomTable.generateStyle()
						break;
					}
					case '%concerns table%': {
						attributes['data-concerns-table'] = SlotCustomTable.generateStyle()
						break;
					}
				}

				writer.appendChild(
					writer.createElement('div', attributes, alias),
					fragment
				);
				data.content = fragment;
			}
		});

		// Processing copied, pasted or dragged content
		this.listenTo(document, 'clipboardOutput', (evt, data) => {
			if (data.content.childCount !== 1) return;
			console.log('table clipboardOutput')

			const viewElement = data.content.getChild(0);

			if (viewElement.is('element', 'div') && viewElement.hasClass(TABLE_CONTAINER)) {
				data.dataTransfer.setData(
					'table-slot-alias',
					JSON.stringify(SlotCustomTable.getDataFromView(viewElement)));
			}
		});
	}

	static getDataFromView(viewElement) {
		const resultData = {
			alias: SlotCustomTable.getText(viewElement),
		}

		if (viewElement.hasAttribute('data-workers-table')) {
			resultData['data-workers-table'] = viewElement.getAttribute('data-workers-table');
		} else if (viewElement.hasAttribute('data-concerns-table')) {
			resultData['data-concerns-table'] = viewElement.getAttribute('data-concerns-table');
		} else if (viewElement.hasAttribute('data-tasks-table')) {
			resultData['data-tasks-table'] = viewElement.getAttribute('data-tasks-table');
		}

		return resultData;
	}

	static getText(viewElement) {
		return Array.from(viewElement.getChildren())
			.map(node => node.is('$text') ? node.data : '' )
			.join('');
	}

	static generateStyle() {
		return JSON.stringify({
			table: {
				width: '100%',
			},
			th: {
				color: 'rgb(0, 0, 0)',
				'font-size': '18px',
				'font-weight': '400',
				'text-align': 'center',
				'font-family': 'Montserrat, sans-serif',
			},
			td: {
				color: 'rgb(0, 0, 0)',
				'font-size': '14px',
				'font-weight': '400',
				'text-align': 'left',
				'font-family': 'Montserrat, sans-serif',
			}
		})
	}

}
