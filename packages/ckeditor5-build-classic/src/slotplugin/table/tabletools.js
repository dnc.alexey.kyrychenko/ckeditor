import { isWidget } from '@ckeditor/ckeditor5-widget';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import { createDropdown, addToolbarToDropdown } from '@ckeditor/ckeditor5-ui/src/dropdown/utils';
import { styles, tableTypes } from './constants';

import AlignCenter from '@ckeditor/ckeditor5-core/theme/icons/align-center.svg';
import AlignLeft from '@ckeditor/ckeditor5-core/theme/icons/align-left.svg';
import AlignRight from '@ckeditor/ckeditor5-core/theme/icons/align-right.svg';
import AlignJustify from '@ckeditor/ckeditor5-core/theme/icons/align-justify.svg';

export default class TableTools {

	static getClosestSelectedTableWidget(selection) {
		const viewElement = selection.getSelectedElement();

		if (viewElement && TableTools.isTableWidget(viewElement)) {
			return viewElement;
		}

		let parent = selection.getFirstPosition().parent;

		while (parent) {
			if (parent.is('element') && TableTools.isTableWidget(parent)) {
				return parent;
			}
			parent = parent.parent;
		}

		return null;
	}

	static isTableWidget(viewElement) {
		return isWidget(viewElement) && (
			viewElement.hasAttribute('data-workers-table')
			|| viewElement.hasAttribute('data-tasks-table')
			|| viewElement.hasAttribute('data-concerns-table')
		)
	}

	static getTableType(alias) {
		switch (alias) {
			case '%workers table%': {
				return tableTypes.workers;
			}
			case '%tasks table%': {
				return tableTypes.tasks;
			}
			case '%concerns table%': {
				return tableTypes.concerns;
			}
		}
	}

	static getStyleByType(tableType, attributes) {
		switch (tableType) {
			case tableTypes.workers: {
				return attributes['data-workers-table'];
			}
			case tableTypes.tasks: {
				return attributes['data-tasks-table'];
			}
			case tableTypes.concerns: {
				return attributes['data-concerns-table'];
			}
		}
	}

	static setTableStyle(tabletType, value, current) {
		switch (tabletType) {
			case styles.width: {
				current.table.width = value;
				break;
			}
			case styles.headerSize: {
				current.th['font-size'] = value;
				break;
			}
			case styles.headerFamily: {
				current.th['font-family'] = value;
				break;
			}
			case styles.headerAlign: {
				current.th['text-align'] = value;
				break;
			}
			case styles.headerWeight: {
				current.th['font-weight'] = value;
				break;
			}
			case styles.headerColor: {
				current.th.color = value;
				break;
			}
			case styles.bodySize: {
				current.td['font-size'] = value;
				break;
			}
			case styles.bodyFamily: {
				current.td['font-family'] = value;
				break;
			}
			case styles.bodyAlign: {
				current.td['text-align'] = value;
				break;
			}
			case styles.bodyWeight: {
				current.td['font-weight'] = value;
				break;
			}
			case styles.bodyColor: {
				current.td.color = value;
				break;
			}
		}
	}

	static updateTableAttributes(tableType, alias, style) {
		const attributes = {
			alias,
		}
		const stringStyle = JSON.stringify(style);

		switch (tableType) {
			case tableTypes.workers: {
				attributes['data-workers-table'] = stringStyle;
				break;
			}
			case tableTypes.tasks: {
				attributes['data-tasks-table'] = stringStyle;
				break;
			}
			case tableTypes.concerns: {
				attributes['data-concerns-table'] = stringStyle;
				break;
			}
		}

		return attributes;
	}

	static generateButton(text, executor, classes = '') {
		const button = new ButtonView()

		button.set({
			label: text,
			withText: true,
		})

		button.on('execute', executor)

		button.extendTemplate({
			attributes: {
				class: [
					classes,
				],
			},
		})

		button.render();

		return button
	}
	static generateFontSizeButtons(editor, isHeader = false) {
		const buttons = [];
		const styleType = isHeader ? styles.headerSize : styles.bodySize;

		for (let i = 10; i < 25; i += 2) {
			const button = TableTools.generateButton(`${i}`, () => {
				editor.execute('changeTableStyle', { styleType, value: `${i}px` })
			}, `ck-fs-${i}`);
			buttons.push(button);
		}

		return buttons;
	}
	static generateFontFamilyButtons(editor, isHeader = false) {
		const buttons = [];
		const styleType = isHeader ? styles.headerFamily : styles.bodyFamily;

		buttons.push(
			TableTools.generateButton('Montserrat (Default)', () => {
				editor.execute('changeTableStyle', { styleType, value: 'Montserrat, sans-serif' })
			}),
			TableTools.generateButton('Arial', () => {
				editor.execute('changeTableStyle', { styleType, value: 'Arial, Helvetica, sans-serif' })
			},  'Arial'),
			TableTools.generateButton('Courier New', () => {
				editor.execute('changeTableStyle', { styleType, value: "'Courier New', Courier, monospace" })
			}, 'Courier-New'),
			TableTools.generateButton('Georgia', () => {
				editor.execute('changeTableStyle', { styleType, value: 'Georgia, serif' })
			}, 'Georgia'),
			TableTools.generateButton('Lucida Sans Unicode', () => {
				editor.execute('changeTableStyle', { styleType, value: "'Lucida Sans Unicode', sans-serif" })
			}, 'Lucida-Sans-Unicode'),
			TableTools.generateButton('Tahoma', () => {
				editor.execute('changeTableStyle', { styleType, value: 'Tahoma, Geneva, sans-serif' })
			}, 'Tahoma'),
			TableTools.generateButton('Times New Roman', () => {
				editor.execute('changeTableStyle', { styleType, value: "'Times New Roman', Times, serif" })
			}, 'Times-New-Roman'),
			TableTools.generateButton('Trebuchet MS', () => {
				editor.execute('changeTableStyle', { styleType, value: "'Trebuchet MS', Helvetica, sans-serif" })
			}, 'Trebuchet'),
			TableTools.generateButton('Verdana', () => {
				editor.execute('changeTableStyle', { styleType, value: 'Verdana, Geneva, sans-serif' })
			}, 'Verdana'),
			TableTools.generateButton('Calibri', () => {
				editor.execute('changeTableStyle', { styleType, value: 'Calibri, sans-serif;' })
			}, 'Calibri'),
		)

		return buttons;
	}
	static generateWidthButtons(editor) {
		const buttons = [];

		for (let i = 1; i < 5; i++) {
			const width = 25 * i;
			buttons.push(TableTools.generateButton(`${width}%`, () => {
				editor.execute('changeTableStyle', { styleType: styles.width, value: `${width}%` })
			}));
		}

		return buttons;
	}
	static generateAlignsButtons(editor, isHeader) {
		const buttons = [];
		const styleType = isHeader ? styles.headerAlign : styles.bodyAlign;

		const config = [
			{
				icon: AlignLeft,
				value: 'left',
			},
			{
				icon: AlignCenter,
				value: 'center',
			},
			{
				icon: AlignRight,
				value: 'right',
			},
			{
				icon: AlignJustify,
				value: 'justify',
			}
		];

		for (let i = 0; i < config.length; i++) {
			const button = new ButtonView();
			const { icon, value } = config[i];

			button.set({
				icon,
			});
			button.on('execute', () => {
				editor.execute('changeTableStyle', { styleType, value })
			});

			buttons.push(button);
		}

		return buttons;
	}
	static generateFontStyleButtons(editor, isHeader) {
		const buttons = [];
		const styleType = isHeader ? styles.headerWeight : styles.bodyWeight;

		buttons.push(TableTools.generateButton('Default', () => {
			editor.execute('changeTableStyle', { styleType, value: '400' })
		}));
		buttons.push(TableTools.generateButton('Bold', () => {
			editor.execute('changeTableStyle', { styleType, value: '600' })
		}));

		return buttons;
	}
	static generateDropdown(locale, config, buttons) {
		const dropdown = createDropdown(locale)

		dropdown.buttonView.set(config);

		addToolbarToDropdown(dropdown, buttons)
		dropdown.toolbarView.isVertical = true;

		return dropdown;
	}

	static checkColorValue(color) {
		if (color === null) {
			color = '#000';
		}
		return color;
	}
	static generateStyleFromObject(obj) {
		const arr = [];
		for(const key in obj) {
			arr.push(`${key}: ${obj[key]};`);
		}
		return arr.join('');
	}

}
