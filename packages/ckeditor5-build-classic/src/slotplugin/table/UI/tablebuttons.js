import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import FontFamilyIcon from '@ckeditor/ckeditor5-core/theme/icons/pencil.svg';
import FontSizeIcon from '@ckeditor/ckeditor5-core/theme/icons/pilcrow.svg';
import AlignCenter from '@ckeditor/ckeditor5-core/theme/icons/align-center.svg';
import BoldIcon from '@ckeditor/ckeditor5-basic-styles/theme/icons/bold.svg';
import TableTools from '../tabletools';

export default class SlotCustomTableButtons extends Plugin {

	init() {
		const { editor } = this;
		const { componentFactory } = editor.ui;

		componentFactory.add(
			'customWidth',
			(locale) => {
				return TableTools.generateDropdown(locale, {
					label: 'Width',
					withText: true,
				}, TableTools.generateWidthButtons(editor))
			}
		)
		componentFactory.add(
			'customHeaderFontFamily',
			(locale) => {
				return TableTools.generateDropdown(locale, {
					label: 'Header Font Family',
					icon: FontFamilyIcon,
					tooltip: true,
				}, TableTools.generateFontFamilyButtons(editor, true))
			}
		)
		componentFactory.add(
			'customBodyFontFamily',
			(locale) => {
				return TableTools.generateDropdown(locale, {
					label: 'Body Font Family',
					icon: FontFamilyIcon,
					tooltip: true,
				}, TableTools.generateFontFamilyButtons(editor, false))
			}
		)
		componentFactory.add(
			'customHeaderFontSize',
			(locale) => {
				return TableTools.generateDropdown(locale, {
					label: 'Header Font Size',
					icon: FontSizeIcon,
					tooltip: true,
				}, TableTools.generateFontSizeButtons(editor,true))
			}
		)
		componentFactory.add(
			'customBodyFontSize',
			(locale) => {
				return TableTools.generateDropdown(locale, {
					label: 'Body Font Size',
					icon: FontSizeIcon,
					tooltip: true,
				}, TableTools.generateFontSizeButtons(editor,false))
			}
		)
		componentFactory.add(
			'customHeaderAlign',
			(locale) => {
				return TableTools.generateDropdown(locale, {
					label: 'Header Alignment',
					icon: AlignCenter,
					tooltip: true,
				}, TableTools.generateAlignsButtons(editor, true))
			}
		)
		componentFactory.add(
			'customBodyAlign',
			(locale) => {
				return TableTools.generateDropdown(locale, {
					label: 'Body Alignment',
					icon: AlignCenter,
					tooltip: true,
				}, TableTools.generateAlignsButtons(editor, false))
			}
		)
		componentFactory.add(
			'customHeaderBold',
			(locale) => {
				return TableTools.generateDropdown(locale, {
					label: 'Header Style',
					icon: BoldIcon,
					tooltip: true,
				}, TableTools.generateFontStyleButtons(editor, true))
			}
		)
		componentFactory.add(
			'customBodyBold',
			(locale) => {
				return TableTools.generateDropdown(locale, {
					label: 'Body Style',
					icon: BoldIcon,
					tooltip: true,
				}, TableTools.generateFontStyleButtons(editor, false))
			}
		)
	}

	static get pluginName() {
		return 'RegisterTableButtons';
	}

}
