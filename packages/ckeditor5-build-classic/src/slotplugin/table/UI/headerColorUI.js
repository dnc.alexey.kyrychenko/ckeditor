import ColorUI from '@ckeditor/ckeditor5-font/src/ui/colorui';
import ColorIcon from '@ckeditor/ckeditor5-font/theme/icons/font-color.svg';
import { colors } from '../constants'

export default class HeaderColorUI extends ColorUI {

	constructor(editor) {

		editor.config.define('TableHeaderColor', {
			colors,
			columns: 5
		} );

		super(editor, {
			commandName: 'changeTableHeaderColor',
			componentName: 'TableHeaderColor',
			icon: ColorIcon,
			dropdownLabel: 'Header color',
		});
	}

	static get pluginName() {
		return 'TableHeaderColor';
	}
}
