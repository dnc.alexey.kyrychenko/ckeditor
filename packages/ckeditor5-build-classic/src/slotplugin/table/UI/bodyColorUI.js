import ColorUI from '@ckeditor/ckeditor5-font/src/ui/colorui';
import ColorIcon from '@ckeditor/ckeditor5-font/theme/icons/font-color.svg';
import { colors } from '../constants'

export default class BodyColorUI extends ColorUI {

	constructor(editor) {

		editor.config.define('TableBodyColor', {
			colors,
			columns: 5
		} );

		super(editor, {
			commandName: 'changeTableBodyColor',
			componentName: 'TableBodyColor',
			icon: ColorIcon,
			dropdownLabel: 'Body color',
		});
	}

	static get pluginName() {
		return 'TableBodyColor';
	}
}
