import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import { WidgetToolbarRepository } from '@ckeditor/ckeditor5-widget';
import TableTools from './tabletools';
import { HeaderColorUI, BodyColorUI } from './UI';

export default class SlotCustomTableToolbar extends Plugin {

	static get requires() {
		return [ WidgetToolbarRepository, HeaderColorUI, BodyColorUI ];
	}

	afterInit() {
		const editor = this.editor;
		const widgetToolbarRepository = editor.plugins.get(WidgetToolbarRepository);

		widgetToolbarRepository.register('custom-table', {
			items: [
				'customWidth',
				'|',
				'customHeaderFontFamily',
				'customHeaderFontSize',
				'customHeaderAlign',
				'customHeaderBold',
				'TableHeaderColor',
				'|',
				'customBodyFontFamily',
				'customBodyFontSize',
				'customBodyAlign',
				'customBodyBold',
				'TableBodyColor',
			],
			getRelatedElement: TableTools.getClosestSelectedTableWidget,
		});
	}

}
