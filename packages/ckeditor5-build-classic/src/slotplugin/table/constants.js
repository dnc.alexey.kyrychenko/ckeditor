export const TABLE_CONTAINER = 'custom-table'
export const TABLE_SCHEMA = 'custom-table-slot'

export const styles = {
	width: 'width',
	headerColor: 'headerColor',
	headerSize: 'headerFontSize',
	headerFamily: 'headerFontFamily',
	headerAlign: 'headerAlign',
	headerWeight: 'headerWeight',
	bodyColor: 'bodyColor',
	bodySize: 'bodyFontSize',
	bodyFamily: 'bodyFontFamily',
	bodyAlign: 'bodyAlign',
	bodyWeight: 'bodyWeight',
}

export const tableTypes = {
	workers: 'workers',
	tasks: 'tasks',
	concerns: 'concerns',
}

export const colors = [
	{
		color: 'rgb(0, 0, 0)',
		label: 'Black'
	},
	{
		color: 'rgb(77, 77, 77)',
		label: 'Dim grey'
	},
	{
		color: 'rgb(153, 153, 153)',
		label: 'Grey'
	},
	{
		color: 'rgb(230, 230, 230)',
		label: 'Light grey'
	},
	{
		color: 'rgb(255, 255, 255)',
		label: 'White',
		hasBorder: true
	},
	{
		color: 'rgb(230, 76, 76)',
		label: 'Red'
	},
	{
		color: 'rgb(230, 153, 76)',
		label: 'Orange'
	},
	{
		color: 'rgb(230, 230, 76)',
		label: 'Yellow'
	},
	{
		color: 'rgb(153, 230, 76)',
		label: 'Light green'
	},
	{
		color: 'rgb(76, 230, 76)',
		label: 'Green'
	},
	{
		color: 'rgb(76, 230, 153)',
		label: 'Aquamarine'
	},
	{
		color: 'rgb(76, 230, 230)',
		label: 'Turquoise'
	},
	{
		color: 'rgb(76, 153, 230)',
		label: 'Light blue'
	},
	{
		color: 'rgb(76, 76, 230)',
		label: 'Blue'
	},
	{
		color: 'rgb(153, 76, 230)',
		label: 'Purple'
	}
]
