import { Command } from '@ckeditor/ckeditor5-core';
import TableTools from '../tabletools';
import { TABLE_SCHEMA } from '../constants';

export default class StyleCommand extends Command {

	execute({ styleType, value }) {
		const { editor } = this;
		const { selection } = editor.model.document
		const currentTable = selection.getSelectedElement()
		const attributes = Object.fromEntries(currentTable.getAttributes())

		const tableType = TableTools.getTableType(attributes.alias)

		if (!tableType) return null;

		const tableStyle = JSON.parse(TableTools.getStyleByType(tableType, attributes))

		TableTools.setTableStyle(styleType, value, tableStyle)

		const updatedAttributes = TableTools.updateTableAttributes(
			tableType,
			attributes.alias,
			tableStyle)

		editor.model.change((writer) => {
			const tableElement = writer.createElement(TABLE_SCHEMA, updatedAttributes)

			editor.model.insertContent(tableElement, selection)
		})
	}

}
