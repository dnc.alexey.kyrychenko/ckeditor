import { styles } from '../constants';
import ColorCommand from './colorcommand';

export default class HeaderColor extends ColorCommand {

	execute({ value }) {
		super.execute({ styleType: styles.headerColor, value });
	}

}
