import { styles } from '../constants';
import ColorCommand from './colorcommand';

export default class BodyColor extends ColorCommand {

	execute({ value }) {
		super.execute({ styleType: styles.bodyColor, value });
	}

}
