import StyleCommand from './stylecommand';

export default class ColorCommand extends StyleCommand {

	execute({ styleType, value }) {
		if (value === null) {
			value = 'rgb(0, 0, 0)';
		}
		super.execute({ styleType, value });
	}

}
