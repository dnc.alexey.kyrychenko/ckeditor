import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import Widget from '@ckeditor/ckeditor5-widget/src/widget';
import UpcastWriter from '@ckeditor/ckeditor5-engine/src/view/upcastwriter';
import { toWidget, viewToModelPositionOutsideModelElement } from '@ckeditor/ckeditor5-widget/src/utils';

export default class SlotPageBreak extends Plugin {
	static get requires() {
		return [ Widget ];
	}

	static get pluginName() {
		return 'Custom page break plugin'
	}

	init() {
		this._defineSchema();
		this._defineConverters();
		this._defineClipboardInputOutput();

		this.editor.editing.mapper.on(
			'viewToModelPosition',
			viewToModelPositionOutsideModelElement(this.editor.model,
				element => element.hasClass('page-break'))
		);
	}

	_defineSchema() {
		this.editor.model.schema.register('page-break', {
			allowIn: '$root',
			isObject: true,
			isInline: false,
		});
	}

	_defineConverters() {
		const conversion = this.editor.conversion;

		// Data-to-model conversion
		conversion.for('upcast').elementToElement({
			view: {
				name: 'div',
				classes: ['page-break']
			},
			model: (_, { writer }) => (
				writer.createElement('page-break', {})
			)
		});

		// Model-to-data conversion
		conversion.for('dataDowncast').elementToElement({
			model: 'page-break',
			view: (_, { writer: viewWriter }) => (
				createPageBreakView(viewWriter)
			)
		});

		// Model-to-view conversion
		conversion.for('editingDowncast')
			.elementToElement({
				model: 'page-break',
				view: (_, { writer: viewWriter }) => (
					toWidget(createPageBreakView(viewWriter), viewWriter)
				)
			});

		function createPageBreakView(viewWriter) {
			const pageBreakView = viewWriter.createContainerElement('div', { class: 'page-break' });
			const textContainer = viewWriter.createContainerElement('span', { class: 'page-break-text'});

			viewWriter.insert(viewWriter.createPositionAt(textContainer, 0), viewWriter.createText('Page Break'));
			viewWriter.insert(viewWriter.createPositionAt(pageBreakView, 0), textContainer);

			return pageBreakView;
		}
	}

	// Integration with the clipboard pipeline
	_defineClipboardInputOutput() {
		const view = this.editor.editing.view;
		const viewDocument = view.document;

		// Processing pasted or dropped content
		this.listenTo(viewDocument, 'clipboardInput', (evt, data) => {
			if (data.content) return;

			const staticSlot = data.dataTransfer.getData('static-slot-alias');

			if (staticSlot) {

				const writer = new UpcastWriter(viewDocument);
				const fragment = writer.createDocumentFragment();

				writer.appendChild(
					writer.createElement('div', { class: 'page-break' }, [
						writer.createElement(
							'span',
							{ class: 'page-break-text' },
							'Page Break')
					]),
					fragment
				);

				data.content = fragment;
			}
		} );

		// Processing copied, pasted or dragged content
		this.listenTo(document, 'clipboardOutput', (evt, data) => {
			if (data.content.childCount !== 1) return;

			const viewElement = data.content.getChild(0);

			if (viewElement.is('element', 'div') && viewElement.hasClass('page-break')) {
				data.dataTransfer.setData('static-slot-alias', JSON.stringify({ alias: 'page-break' }));
			}
		});
	}
}
